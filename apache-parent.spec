Name:           apache-parent
Version:        27
Release:        1
Summary:        Maven Parent POM file for Apache projects
License:        ASL 2.0
URL:            http://apache.org/
Source0:        https://repo1.maven.org/maven2/org/apache/apache/%{version}/apache-%{version}-source-release.zip
BuildArch:      noarch

BuildRequires:  maven-local mvn(org.apache.maven.plugins:maven-enforcer-plugin)
BuildRequires:  mvn(org.apache.maven.plugins:maven-remote-resources-plugin) mvn(org.apache:apache-jar-resource-bundle)
Requires:       mvn(org.apache:apache-jar-resource-bundle)

%description
Maven parent POM (or super POM) is used to structure the project to avoid
redundancies or duplicate configurations using inheritance between pom
files. It helps in easy maintenance in long term.

%prep
%setup -q -n apache-%{version}

%pom_remove_plugin :maven-site-plugin

%build
%mvn_build

%install
%mvn_install

%files -f .mfiles
%doc LICENSE NOTICE

%changelog
* Thu Oct 10 2024 zhaosai<zhaosaisai@kylinos.cn> - 27-1
- upgrade to 27

* Tue Dec 22 2020 Ge Wang<wangge20@huawei.com> -19-5
- Modify Source0 url

* Thu Dec 12 2019 gulining<gulining1@huawei.com> - 19-4
- Pakcage init
